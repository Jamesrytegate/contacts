<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\testController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'],function(){
    Route::post('register',[AuthController::class,'register']);
    Route::post('login',[AuthController::class,'login']);
});

Route::group(['prefix' => 'contacts','middleware'=>'auth:api'],function(){
    Route::post('add',[ContactController::class,'add']);
    Route::post('update',[ContactController::class,'update']);
    Route::post('remove/{contact_id}',[ContactController::class,'remove']);
    Route::get('/{contact_id}',[ContactController::class,'show']);
    Route::get('',[ContactController::class,'index']);
    Route::post('/next-of-kin',[ContactController::class,'addNextofkin']);
});
