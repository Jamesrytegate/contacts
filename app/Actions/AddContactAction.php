<?php
namespace App\Actions;

use App\Http\Requests\AddContactRequest;
use App\Models\Contact;

class AddContactAction{
    /**
     * Adds a contact to the db
     *
     * @param AddContactRequest $request
     * @return Contact
     */
    public function execute(AddContactRequest $request) : Contact{
        return Contact::create($request->validated());
    }
}