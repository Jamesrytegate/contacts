<?php

namespace App\Http\Controllers;

use App\Classes\ContactObj;
use App\Http\Requests\AddContactRequest;
use App\Http\Requests\GetContactsRequest;
use App\Http\Requests\NextOfKinRequest;
use App\Http\Requests\UpdateContactRequest;

class ContactController extends Controller
{
    private ContactObj $contact;

    public function __construct(ContactObj $contact){
        $this->contact = $contact;
    }

    public function add(AddContactRequest $request){
        return $this->contact->add($request);
    }

    public function remove(int $contact_id){
        $this->contact->remove($contact_id);
        return response('contact successfully removed',200);
    }

    public function update(UpdateContactRequest $request){
        return $this->contact->update($request);
    }

    public function index(GetContactsRequest $request){
        return $this->contact->getAll($request);
    }

    public function show(int $contact_id){
        return $this->contact->find($contact_id);
    }

    public function addNextofkin(NextOfKinRequest $request){
        return $this->contact->addNextOfKin($request);
    }
}
