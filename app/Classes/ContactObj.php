<?php
namespace App\Classes;

use App\Actions\AddContactAction;
use App\Http\Requests\AddContactRequest;
use App\Http\Requests\GetContactsRequest;
use App\Http\Requests\NextOfKinRequest;
use App\Http\Requests\UpdateContactRequest;
use App\Models\Contact;
use App\Models\NextOfKin;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ContactObj{
    /**
     * adds contact to db
     *
     * @param AddContactRequest $request
     * @return Contact
     */
    public function add(AddContactRequest $request) : Contact{
        //return Contact::create($request->validated());
        $contact = new Contact();
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->phone_number = $request->phone_number;
        $contact->email = $request->email;
        $contact->address = $request->address;
        $contact->save();

        return $contact;
    }

    /**
     * removes a contact
     *
     * @param integer $contact_id
     * @return void
     */
    public function remove(int $contact_id) : void{
        if(!Contact::where('id',$contact_id)->first()){
            throw new Exception('Contact does not exists');
        }

        Contact::destroy($contact_id);
    }

    /**
     * fetches all contacts
     *
     * @param GetContactsRequest $request
     * @return Collection
     */
    public function getAll(GetContactsRequest $request){
        $length = $request->length ?? 5;
        return Contact::with('nextOfKin')->paginate($length);
    }

    /**
     * Retrieves a contact
     *
     * @param integer $contact_id
     * @return Contact
     */
    public function find(int $contact_id) : Contact{
        return Contact::with('nextOfKin')->where('id',$contact_id)->first();
    }

    /**
     * updates a contact
     *
     * @param UpdateContactRequest $request
     * @return Contact
     */
    public function update(UpdateContactRequest $request) : Contact{
        $contact = Contact::find($request->contact_id);

        $contact->update($request->validated());

        return $contact;
    }

    public function addNextOfKin(NextOfKinRequest $request) : NextOfKin{
        return NextOfKin::create($request->validated());
    }
}